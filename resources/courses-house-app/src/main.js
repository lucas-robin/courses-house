import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vuelidate from 'vuelidate'
import axios from 'axios'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faBars, faCheck, faArrowCircleLeft } from '@fortawesome/free-solid-svg-icons'
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import VueCarousel from 'vue-carousel'
import vueSmoothScroll from 'vue2-smooth-scroll'

// Feuilles de styles
import "@/assets/styles/main.css";
import "@/assets/styles/fonts.css";
import "@/assets/styles/helpers.css";
import "animate.css";

// Plugin
Vue.use(Vuelidate)
Vue.use(VueCarousel)
Vue.use(vueSmoothScroll)

console.log(faCheckCircle);


// Ajout des icônes
library.add(faBars, faCheck, faArrowCircleLeft, faCheckCircle)

Vue.config.productionTip = false

// Props globales
Vue.prototype.$axios = axios

// Composants globlaux
Vue.component('font-awesome-icon', FontAwesomeIcon)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#divWpVue')
