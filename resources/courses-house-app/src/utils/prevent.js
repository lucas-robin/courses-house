// Enlève directement le caractère saisi dans le champ input de type text, si celui-ci est un nombre ou un caractère spécial ou un espace @keypress
export function preventNumberKeys(event) {
    const regex = /^[A-Za-z\é\ç\è\ê\à\â\á\â\ù\ú\ô\û\ë\ä\ö\-\É\È\À\Â\Á]+$/
    if (regex.test(event.key)) {
        return true
    } else {
        // Empêche l'évenement @keypress de se déclencher
        event.preventDefault()
    }
}

// Empêche un champ de saisir plus de [limit] caractères
export function preventExceedSizePostal(event, limit, number) {
    if (String(number).length > limit) {
        event.preventDefault()
    }
}

// Empêche de saisir les caratère spéciaux (ex: "%$()°>)
export function preventSpecialKeys(event) {
    const regex = /^[0-9A-Za-z\é\ç\è\ê\à\â\á\â\ù\ú\ô\û\ë\ä\ö\-\É\È\À\Â\Á\ ]*$/
    if (regex.test(event.key)) {
        return true
    } else {
        // Empêche l'évenement @keypress de se déclencher
        event.preventDefault()
    }
}

export default {
    preventNumberKeys,
    preventExceedSizePostal,
    preventSpecialKeys
}