import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import NotFound from '../views/NotFound.vue'
import PaymentForm from '@/components/PaymentForm.vue'
import DeliveryCardSlider from '@/components/DeliveryCardSlider.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    children: [
      {
        path: '',
        name: 'slider',
        component: DeliveryCardSlider,
      },
      {
        path: '/formulaire',
        name: 'formulaire',
        component: PaymentForm,
      },
      { path: '/404', component: NotFound },  
      { path: '*', redirect: '/404' },
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
