import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    formData: {},
    deliveries: [],
  },
  getters: {
    getDeliveries: state => {
      return state.deliveries
    }
  },
  mutations: {
    // Met à jour l'intégralité du formulaire
    updateFormData(state, data) {
      Object.assign(state.formData, {...data});
    },
    updateDeliveries(state, deliveries) {
      state.deliveries = deliveries
    }
  },
  actions: {
    async fetchDeliveries({commit}) {
      const {data} = await axios.get(`${process.env.VUE_APP_API_BASE_URL}/wp-json/wp/v2/delivery`)
      const deliveries = data.map(d => {
        return {
          id: d.id,
          title: d.title.rendered,
          description: d.content.rendered,
          price: d.price,
          color: d.color,
        }
      }) 
      commit('updateDeliveries', deliveries)
    },
    async submitForm({state: { formData }}) {
      const form = Object.assign({}, formData)
      delete form.optin
      const res = await axios.post(`${process.env.VUE_APP_API_BASE_URL}/wp-json/delivery/buy`, JSON.stringify(form))
    }
  }
})
