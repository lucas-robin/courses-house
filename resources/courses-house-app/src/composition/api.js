import { ref } from 'vue';
import axios, { NotFoundError } from 'axios';

export function useAxiosGet(url, headers = {}) {
    const loading = ref(false);
    const data = ref([]);
    const error = ref(false);
    const fetchData = async function () {
        loading.value = true;
        try {
            data.value = await axios.get(url, { 
                headers, 
            });
        } catch(e) {
            if (e instanceof NotFoundError) {
                error.value = "La route est introuvable...";
            }
        }
        loading.value = false;
    }
    return {
        loading,
        data,
        error,
        fetchData
    }
}

