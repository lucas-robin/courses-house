// vue.config.js
module.exports = {
	chainWebpack: config => {
		// Not naming bundle 'app'
		config.entryPoints.delete('app');
	},
	// Overriding webpack config
	css: {
		extract: {
			filename: 'css/style-vue.css',
			chunkFilename: 'css/style-vue-vendor.css',
		},
	},
	configureWebpack: {
		// Naming bundle 'bundleName'
		entry: {
			bundleName: './src/main.js'
		},
		output: {
			filename: 'js/app-vue.js',
			chunkFilename: 'js/app-vue-vendor.js'
		},
	}
};