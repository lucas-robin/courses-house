<?php 


// Ajouter la prise en charge des images mises en avant
add_theme_support( 'post-thumbnails' );

// Ajouter automatiquement le titre du site dans l'en-tête du site
add_theme_support( 'title-tag' );


// Debug Mail
function action_wp_mail_failed($wp_error) 
{
    return error_log(print_r($wp_error, true));
    
} 
add_action('wp_mail_failed', 'action_wp_mail_failed', 10, 1);

// Créer une page option
if( function_exists('acf_add_options_sub_page') ) {

    // Add parent.
    $parent = acf_add_options_page(array(
        'page_title'  => __('Options de Courses House'),
        'menu_title'  => __('Courses House'),
        "menu_slug"     => "settings-courses-house",
        "capability"    => "edit_posts",
        'redirect'    => false,
    ));
}

// Function to change email address
function wpb_sender_email( $original_email_address ) {
    return 'no-reply@courses-house.fr';
}

// Function to change sender name
function wpb_sender_name( $original_email_from ) {
    return 'Courses House';
}

// Hooking up our functions to WordPress filters 
add_filter( 'wp_mail_from', 'wpb_sender_email' );
add_filter( 'wp_mail_from_name', 'wpb_sender_name' );