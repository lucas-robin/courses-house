<?php
/**
 * Register custom post types
 *
 * @package Delivery
 */
namespace Delivery;
class RegisterPostTypes {
    /**
    * Run the actions
    */
    public function run() {
        add_action('init', array($this, 'register_delivery_post_type'));
    }
    /**
     * Register Delivery Post Type
    *
    * @access public
    * @return void
    */
    public function register_delivery_post_type() {
        register_post_type( 'delivery',
            array(
                'labels' => array(
                    'name'          => __( 'Prestations' ),
                    'singular_name' => __( 'Prestation' ),
                    'all_items'     => __( 'Toute les prestations' ),
                    'add_new'       => __( 'Ajouter une prestation' ),
                    'add_new_item'  => __( 'Créer une nouvelle prestation' ),
                    'edit_item'     => __( 'Modifier la prestation' ),
                ),
                'exclude_from_search' => false,
                'has_archive'         => true,
                'public'              => true,
                'rewrite'             => true,
                'show_in_nav_menus'   => false,
                'show_ui'             => true,
                'publicly_queryable'  => true,
                'supports'            => array('title', 'editor', 'thumbnail'),
                'menu_icon'   => 'dashicons-welcome-write-blog'
            )
        );
    }

}