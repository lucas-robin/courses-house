<?php
/**
 * Plugin Name: Courses House Delivery
 * Description: CPT pour la gestion et création des prestations proposées par Courses House
 * Version: 1.0.1
 * Author: Lucas Robin
 * Author URI: https://k-lu.fr
 */

// If this file is called directly, abort.
if ( !defined( 'WPINC' ) ) {
    die;
}

use Delivery\RegisterPostTypes;

// Autoload function
spl_autoload_register( 'plugin_autoloader_delivery' );

function plugin_autoloader_delivery( $class_name ) {
    if ( false !== strpos( $class_name, 'Delivery' ) ) {
        $classes_dir = realpath( plugin_dir_path( __FILE__ ) ) . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR;
        $class_file = str_replace( '\\', DIRECTORY_SEPARATOR, $class_name ) . '.php';
        require_once $classes_dir . $class_file;
    }
}

// Initiate functionality
add_action( 'plugins_loaded', 'plugin_init_delivery' );
function plugin_init_delivery() {
    (new RegisterPostTypes)->run();
}