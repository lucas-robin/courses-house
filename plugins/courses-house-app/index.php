<?php
/**
 * Plugin Name: Courses House Application
 * Description: Application 'One Page' de Courses House
 * Version: 1.0.0
 * Author: Lucas Robin
 */


//Register scripts to use
function func_load_vuescripts() {
	wp_register_script('app-vue-vendor', plugin_dir_url( __FILE__ ).'/dist/js/app-vue-vendor.js');
	wp_register_script('app-vue', plugin_dir_url( __FILE__ ).'dist/js/app-vue.js', 'app-vue-vendor', true );
	wp_enqueue_style( 'style-vue-vendor', plugin_dir_url( __FILE__ ).'dist/css/style-vue-vendor.css' );
	wp_enqueue_style( 'style-vue', plugin_dir_url( __FILE__ ).'dist/css/style-vue.css' );
}
// Tell WordPress to register the scripts
add_action('wp_enqueue_scripts', 'func_load_vuescripts');


// Return string for shortcode
function func_wp_vue() {
  	// Add Vue.js
	wp_enqueue_script('app-vue-vendor');

  	// Add my code to it
	wp_enqueue_script('app-vue');

  	// Build String
	$str= "<div id='divWpVue'>Si ce message s'affiche, contacter l'administrateur de ce site.</div>";

	return $str;
} 

add_shortcode( 'wpvue', 'func_wp_vue' );




