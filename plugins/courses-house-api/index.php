<?php
/**
 * Plugin Name: Courses House API
 * Description: API REST de Courses House
 * Version: 1.0.0
 * Author: Lucas Robin
 */

// Règle le problème de Cross Origin
// TODO: DELETE en production
header("Access-Control-Allow-Origin: *");

add_action( 'init', 'my_custom_post_type_rest_support', 25);
add_action( 'rest_api_init', function () {

	// /wp-json/delivery/buy
	// Route envois du mail pour le formulaire
	register_rest_route( 'delivery', '/buy', [
		'methods' => 'POST',
		'callback' => __NAMESPACE__ . '\\sendMails',
	]);

	// /wp-json/wp/v2/delivery?per_page=15
	// Ajouter le champ 'price' de mon CPT delivery dans la reponse JSON de l'API lors de l'appel de la route
	register_rest_field( 'delivery', 'price', array(
        'get_callback' => function( $post_arr ) {
            return get_post_meta( $post_arr['id'], 'price', true );
        },
	));
	
	// Ajouter le champ 'color' de mon CPT delivery dans la reponse JSON de l'API lors de l'appel de la route
	register_rest_field( 'delivery', 'color', array(
        'get_callback' => function( $post_arr ) {
            return get_post_meta( $post_arr['id'], 'color', true );
        },
    ));
});

/**
 * Hook permettant d'obenir les CPT Delivery via /wp-json/wp/v2/delivery
 * @return void
 */
function my_custom_post_type_rest_support() :void {

	global $wp_post_types;
	$post_type_name = 'delivery';

	if( isset( $wp_post_types[ $post_type_name ] ) ) {
		$wp_post_types[$post_type_name]->show_in_rest = true;
		$wp_post_types[$post_type_name]->rest_base = $post_type_name;
		$wp_post_types[$post_type_name]->rest_controller_class = 'WP_REST_Posts_Controller';
	}
}

/**
 * Envois du mail contenant les données du formulaire
 * @param WP_REST_Request $request
 * @return string JSON Encoded for testing
 */
function sendMails( \WP_REST_Request $request ) {

	// Récupération des données du formulaire
	$data = $request->get_body();
	$formData = json_decode($data, true);

	// Traitement du formulaire
	$form = sanitizeFormData($formData);
	$isValid = validateForm($form);

	// Templating du mail à destination de l'admin
	ob_start();
	include("mail/admin.php");
	$templateMailForAdmin = ob_get_clean();

	// Templating du mail à destination du client
	ob_start();
	include("mail/customer.php");
	$templateMailForCustomer = ob_get_clean();

	// Boîte de reception
	$adminMailbox = get_field('mailbox', 'options');
	$customerMailbox = $form['mail'];

	error_log("Mail ADMIN recevant l'achat : " . $adminMailbox);
	error_log("Mail CLIENT recevant le détail : " . $customerMailbox);

	// define headers
	$headers = [];
	
	// $headers[] = 'From:noreply@coursesh.com';
	// $headers[] = 'Reply-To:noreply@courseh.com';
	// $headers[] = 'From: Me Myself <me@example.net>';

	$headers[] = 'MIME-Version:1.0';
	$headers[] = 'Content-Type:text/html';
	$headers[] = 'charset=utf-8';

	// Réponse envoyée au client
	$response = [
		'status' => null,
		'sended' => false,
		'error' => null
    ];

	// Sujet des mails
	$subjectAdmin = "Nouvelle commande: " . strtoupper($form['title']);
	$subjectCustomer = "Courses House : Récapitulatif de votre commande";

	if ($isValid) {
		// Envoi du mail à l'admin et au client
		$isSendedToAdmin = wp_mail($adminMailbox, $subjectAdmin, $templateMailForAdmin, $headers );
		$isSendedToCustomer = wp_mail($customerMailbox, $subjectCustomer, $templateMailForCustomer, $headers );

		if ($isSendedToAdmin && $isSendedToCustomer) {
		// if($isSendedToAdmin) {
			$response['status'] = 200;
			$response['sended'] = true;
			$res = rest_ensure_response(wp_json_encode($response));
			return $res;
		} else { // Si le mail n'a pas été envoyé...
			$response['status'] = 500;
			$response['sended'] = false;
			$response['error'] = 'Error : Mail has been not send.';
			$res = rest_ensure_response(wp_json_encode($response));
			return $res;
		}
	}
}

/**
 * @param array $raw - Data de la candidature brut, reçu du client
 * Format et nettoie les données de la candidature
 */
function sanitizeFormData($raw) :array {

	// Candidature traité à retourner en sortie
	$form = [];

	$form['firstname'] = strtoupper(filter_var($raw['firstname'], FILTER_SANITIZE_STRING));
	$form['lastname'] = strtoupper(filter_var($raw['lastname'], FILTER_SANITIZE_STRING));
	$form['mail'] = filter_var($raw['mail'], FILTER_SANITIZE_EMAIL);
	$form['phone'] = filter_var($raw['phone'], FILTER_SANITIZE_NUMBER_INT);
	$form['message'] = filter_var($raw['message'], FILTER_SANITIZE_STRING);
	$form['address'] = filter_var($raw['address'], FILTER_SANITIZE_STRING);
	$form['city'] = filter_var($raw['city'], FILTER_SANITIZE_STRING);
	$form['postal'] = filter_var($raw['postal'], FILTER_SANITIZE_NUMBER_INT);
	$form['title'] = filter_var($raw['title'], FILTER_SANITIZE_STRING);
	$form['price'] = filter_var($raw['price'], FILTER_SANITIZE_NUMBER_INT);

	return $form;
}

function validateForm($form) {
	$isValid = true;
	if(is_array($form)) {
		foreach ($form as $field) {
			if(empty($field)) {
				$isValid = false;
				break;
			}
		}
		return $isValid;
	}
}

